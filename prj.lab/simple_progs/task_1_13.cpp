﻿#include <iostream>

int main()
{
	setlocale(LC_ALL, "Russian");
	/*13. Ученику 1-го класса назначается дополнительно стакан молока (200 мл), если его вес составляет меньше 30 кг. Определить,
	сколько литров молока потребуется ежедневно для одного класса,
	состоящего из n учеников. После взвешивания вес каждого ученика
	вводится в компьютер.*/
	int Vmilk = 0, kollUch, massUch;
	std::cin >> kollUch;
	for (int a = 0; a < kollUch; a++) {
		std::cin >> massUch;
		if (massUch > 30) {
			Vmilk += 1;
		}
	}
	if (Vmilk % 5 == 0) {
		std::cout << Vmilk / 5 << std::endl;
	}
	else {
		std::cout << Vmilk / 5 + 1 << std::endl;
	}
	system("pause");
	return 0;
}

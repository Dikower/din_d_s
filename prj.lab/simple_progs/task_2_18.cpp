﻿#include <iostream>
#include <math.h>

int main()
{
	/*18. В компьютер вводятся координаты n точек, лежащих на плоскости. Напечатать номер точки, ближайшей к началу координат, и
	величину расстояния от нее до начала координат.*/
	setlocale(LC_ALL, "Russian");
	double n, minS, S, number, x, y;
	std::cin >> n;
	std::cin >> x >> y;
	minS = sqrt(x * x + y * y);
	number = 1;
	for (int a = 1; a < n; a++) {
		std::cin >> x >> y;
		S = sqrt(x * x + y * y);
		if (S < minS) {
			minS = S;
			number = a + 1;
		}
	}
	std::cout << "Номер точки: " << number << ".  Величина расстояния от нее до начала координат: " << minS << std::endl;
	system("pause");
	return 0;
}


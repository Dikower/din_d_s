#include <iostream>

int main() {
	double S = 0;
	for (double a = 1; a <= 56; a++) {
		S = S + (a * 2) / (a * 2 + 1);
	}
	std::cout << S << std::endl;
	return 0;
}
#include <matrixvs/matrixvs.h>
#include <stdexcept>

float& MatrixVs::at(const std::ptrdiff_t row_i, const std::ptrdiff_t col_i) {
  if (row_i < 0 || n_row_ <= row_i) {
    throw std::out_of_range("MatrixVs::at - row index out of range");
  }
  if (col_i < 0 || n_col_ <= col_i) {
    throw std::out_of_range("MatrixVs::at - column index out of range");
  }
  const std::ptrdiff_t i = row_i * n_col_ + col_i;
  return data_[i];
}


float MatrixVs::at(const std::ptrdiff_t row_i, const std::ptrdiff_t col_i) const {
  if (row_i < 0 || n_row_ <= row_i) {
    throw std::out_of_range("MatrixVs::at const - row index out of range");
  }
  if (col_i < 0 || n_col_ <= col_i) {
    throw std::out_of_range("MatrixVs::at const - column index out of range");
  }
  const std::ptrdiff_t i = row_i * n_col_ + col_i;
  return data_[i];
}

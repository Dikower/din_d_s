#include <iostream>
#include <sstream>


struct Complex {
  Complex() {}

  explicit Complex(const double real);

  Complex(const double real, const double imaginary);

  bool operator==(const Complex &rhs) const {
    return (re == rhs.re) && (im == rhs.im);
  }

  bool operator!=(const Complex &rhs) const { return !operator==(rhs); }

  Complex &operator+=(const Complex &rhs);

  Complex &operator+=(const double rhs) { return operator+=(Complex(rhs, rhs)); }

//   Complex& operator-=(const Complex& rhs);
//   Complex& operator-=(const double rhs) { return operator-=(Complex(rhs)); }
//   Complex& operator*=(const Complex& rhs);
  Complex &operator*=(const double rhs);
  Complex &operator*=(const Complex &rhs);
  Complex &operator-=(const double rhs);
  Complex &operator-=(const Complex &rhs);

  std::ostream &writeTo(std::ostream &ostrm) const;

  std::istream &readFrom(std::istream &istrm);

  double re{0.0};
  double im{0.0};

  static const char leftBrace{'{'};
  static const char separator{','};
  static const char rightBrace{'}'};
};

Complex operator+(const Complex &lhs, const Complex &rhs);

Complex operator-(const Complex &lhs, const Complex &rhs);

inline std::ostream &operator<<(std::ostream &ostrm, const Complex &rhs) {
  return rhs.writeTo(ostrm);
}

inline std::istream &operator>>(std::istream &istrm, Complex &rhs) {
  return rhs.readFrom(istrm);
}


Complex::Complex(const double real)
    : Complex(real, 0.0) {
}

Complex::Complex(const double real, const double imaginary)
    : re(real), im(imaginary) { // TODO ask
}

Complex &Complex::operator+=(const Complex &rhs) {
  re += rhs.re;
  im += rhs.im;
  return *this;
}

Complex operator+(const Complex &lhs, const Complex &rhs) {
  Complex sum(lhs);
  sum += rhs;
  return sum;
}


Complex operator-(const Complex &lhs, const Complex &rhs) {
  return Complex(lhs.re - rhs.re, lhs.im - rhs.im);
}

Complex &Complex::operator*=(const double rhs) { // TODO ask
  re *= rhs;
  im *= rhs;
  return *this;
}


std::ostream &Complex::writeTo(std::ostream &ostrm) const {
  ostrm << leftBrace << re << separator << im << rightBrace;
  return ostrm;
}

std::istream &Complex::readFrom(std::istream &istrm) {
  char leftBrace(0);
  double real(0.0);
  char comma(0);
  double imaganary(0.0);
  char rightBrace(0);
  istrm >> leftBrace >> real >> comma >> imaganary >> rightBrace;
  if (istrm.good()) {
    if ((Complex::leftBrace == leftBrace) && (Complex::separator == comma) &&
        (Complex::rightBrace == rightBrace)) {
      re = real;
      im = imaganary;
    } else {
      istrm.setstate(std::ios_base::failbit);
    }
  }
  return istrm;
}

Complex &Complex::operator*=(const Complex &rhs) {
  re *= rhs.re;
  im *= rhs.im;
  return *this;
}

Complex &Complex::operator-=(const double rhs) {
  re -= rhs;
  im -= rhs;
  return *this;
}

Complex &Complex::operator-=(const Complex &rhs) {
  re -= rhs.re;
  im -= rhs.im;
  return *this;
}

// ApproximateEquality
bool AE(double a, double b){
  return std::abs(a - b) < 0.0001;
}

bool testParse(const std::string &str) {
  std::istringstream istrm(str);
  Complex z;
  istrm >> z;
  if (istrm.good()) {
    std::cout << "Read success: " << str << " -> " << z << std::endl;
  } else {
    std::cout << "Read error : " << str << " -> " << z << std::endl;
  }
  return istrm.good();
}

bool testMultiply(double re, double im, double re1, double im1){
  bool ok = true;
  Complex c1 = Complex(re, im);
  Complex c2 = Complex(re1, im1);
  c1 *= c2;
  if (AE(c1.re, re * re1) && AE(c1.im, im * im1)){
    std::cout << "Multiply *= Complex -> ok" << std::endl;
  } else {
    std::cout << "Multiply *= Complex -> wrong" << std::endl;
    ok = false;
  }
  c2 *= re1;
  if (AE(c2.re, re1 * re1) && AE(c2.im, im1 * re1)) {
    std::cout << "Multiply *= double -> ok" << std::endl;
  } else {
    std::cout << "Multiply *= double -> wrong" << std::endl;
    ok = false;
  }
  return ok;
}

bool testMinus(double re, double im, double re1, double im1){
  bool ok = true;
  Complex c1 = Complex(re, im);
  Complex c2 = Complex(re1, im1);
  c1 -= c2;
  if (AE(c1.re, re - re1) && AE(c1.im, im - im1)){
    std::cout << "Complex -= Complex -> ok" << std::endl;
  } else {
    std::cout << "Complex -= Complex -> wrong" << std::endl;
    ok = false;
  }
  c2 -= re1;
  if (AE(c2.re, re1 - re1) && AE(c2.im, im1 - re1)) {
    std::cout << "Complex -= double -> ok" << std::endl;
  } else {
    std::cout << "Complex -= double -> wrong" << std::endl;
    ok = false;
  }
  return ok;
}

bool testPlus(double re, double im, double re1, double im1){
  bool ok = true;
  Complex c1 = Complex(re, im);
  Complex c2 = Complex(re1, im1);
  c1 += c2;
  if (AE(c1.re, re + re1) && AE(c1.im, im + im1)){
    std::cout << "Complex += Complex -> ok" << std::endl;
  } else {
    std::cout << "Complex += Complex -> wrong" << std::endl;
    ok = false;
  }
  c2 += re1;
  if (AE(c2.re, re1 + re1) && AE(c2.im, im1 + re1)) {
    std::cout << "Complex += double -> ok" << std::endl;
  } else {
    std::cout << "Complex += double -> wrong" << std::endl;
    ok = false;
  }
  return ok;
}

int main() {
  Complex z;
  z += Complex(8.0);
  testParse("{8.9,9}");
  testParse("{8.9, 9}");
  testParse("{8.9,9");
  testMultiply(1.2, 1.5, 2.3, 6.3);
  testMinus(1.2, 1.5, 2.3, 6.3);
  testPlus(1.2, 1.5, 2.3, 6.3);

  return 0;
}
#include <iostream>
#include <sstream>
#include <vector>

template <typename T>
class Matrix: T{

  unsigned nRows = 0;
  unsigned nCols = 0;
  std::vector<std::vector<T>> matrix;

  Matrix() {}
  Matrix(unsigned nRows, unsigned nCols){
    nRows = nRows;
    nCols = nCols;
    matrix(nRows);
    for (int i = 0; i < nRows; i++){
      matrix[i].resize(nCols);
    }
  }


  Matrix& operator+(double b);
  Matrix& operator-(double b);
  Matrix& operator*(double b);
  Matrix& operator/(double b);

//  Matrix& operator+(&Matrix another);
//  Matrix& operator-(&Matrix another);
//  Matrix& operator*(&Matrix another);
//  Matrix& operator/(&Matrix another);
};
